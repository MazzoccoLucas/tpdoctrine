<?php
# set-reponse2.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine\Entity\Answer;

$reponse = new Answer();
$reponse->setId("2");
$reponse->setWording("Non, peut mieux faire");

$entityManager->flush();