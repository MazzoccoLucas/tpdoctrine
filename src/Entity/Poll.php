<?php
# src/Entity/Poll.php

namespace tpdoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="Poll")
*/
class Poll
{

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $id;

    /**
    * @ORM\Column(type="text")
    */
    protected $title;

    /**
    * @ORM\OneToMany(targetEntity=Poll::class, cascade={"persist", "remove"}, mappedBy="Answer"))
    */
    protected $created;



    public function __toString()
    {
        $format = "Poll (id: %s, title: %s, created: %s)\n";
        return sprintf($format, $this->id, $this->title, $this->created);
    }


    // getters et setters à générer
    
      public function getId()
    {
        return $this->id;
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
      public function getTitle()
    {
        return $this->title;
    }
     
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function getCreated()
    {
        return $this->created;
    }
     
    public function setCreated($created)
    {
        $this->created = $created;
    }
}
