<?php
# src/Entity/Question.php

namespace tpdoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="questions")
*/
class Question
{

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $id;

    /**
    * @ORM\Column(type="text")
    */
    protected $wording;

    /**
    * @ORM\OneToMany(targetEntity=Answer::class, cascade={"persist", "remove"}, mappedBy="question"))
    */
    protected $answers;

    /**
    * @ORM\OneToMany(targetEntity=Poll::class, cascade={"persist", "remove"}, mappedBy="Answer"))
    */
    protected $idPoll;



    public function __toString()
    {
        $format = "Question (id: %s, wording: %s)\n";
        return sprintf($format, $this->id, $this->wording, $this->answers);
    }

        // le constructeur crée la collection de réponses
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }


    // getters et setters à générer
    
      public function getId()
    {
        return $this->id;
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
      public function getWording()
    {
        return $this->wording;
    }
     
    public function setWording($wording)
    {
        $this->wording = $wording;
    }
    public function getAnswers()
    {
        return $this->answers;
    }
     
    public function addAnswer(Answer $answer)
    {
        $this->answers->add($answer);
        $answer->setQuestion($this);
    }
}
