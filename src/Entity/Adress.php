<?php
# src/Entity/Adress.php

namespace tpdoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(
    name="addresses",
    indexes={
        @ORM\Index(name="search_zipcode", columns={"zipcode"}),
        @ORM\Index(name="search_city", columns={"city"}),
        @ORM\Index(name="search_country", columns={"country"})
    }
 )
*/

class Adress
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $idA;

    /**
    * @ORM\Column(type="integer")
    */
    protected $appart;

    /**
    * @ORM\Column(type="string")
    */
    protected $street;

    /**
    * @ORM\Column(type="string")
    */
    protected $zipcode;

    /**
    * @ORM\Column(type="string")
    */
    protected $city;

    /**
    * @ORM\Column(type="string")
    */
    protected $country;

    // ...


    public function getIdA()
    {
        return $this->idA;
    }
     
    public function setIdA($id)
    {
        $this->idA = $id;
    }
     
    public function getAppart()
    {
        return $this->appart;
    }
     
    public function setAppart($Appart)
    {
        $this->appart = $Appart;
    }
     
    public function getStreet()
    {
        return $this->street;
    }
     
    public function setStreet($Street)
    {
        $this->street = $Street;
    }
     
    public function getZipcode()
    {
        return $this->zipcode;
    }
     
    public function setZipcode($Zipcode)
    {
        $this->zipcode = $Zipcode;
    }
    public function getCity()
    {
        return $this->zipcode;
    }
     
    public function setCity($City)
    {
        $this->city = $City;
    }
    public function getCountry()
    {
        return $this->country;
    }
     
    public function setCountry($Country)
    {
        $this->country = $Country;
    }
    
    public function __toString()
{
    $format = "User (idA: %s, appart: %s, street: %s, zipcode: %s, city: %s, country: %s)\n";
    return sprintf($format, $this->idA, $this->appart, $this->street, $this->zipcode, $this->city, $this->country);
}

}
