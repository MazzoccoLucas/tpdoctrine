<?php
# src/Entity/Answer.php

namespace tpdoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="answers")
*/
class Answer
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $id;

    /**
    * @ORM\Column(type="string")
    */
    protected $wording;

    /**
    * @ORM\ManyToOne(targetEntity=Question::class)
    */
    protected $question;

    public function __toString()
    {
        $format = "Answer (id: %s, wording: %s)\n";
        return sprintf($format, $this->id, $this->wording);
    }

    // getters et setters à générer

  public function getId()
    {
        return $this->id;
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
      public function getWording()
    {
        return $this->wording;
    }
     
    public function setWording($wording)
    {
        $this->wording = $wording;
    }