<?php
# set-reponse1.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine\Entity\Answer;

$reponse = new Answer();
$reponse->setId("1");
$reponse->setWording("Oui, bien sûr !");

$entityManager->flush();