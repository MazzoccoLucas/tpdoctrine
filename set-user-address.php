<?php
# set-user-address.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine\Entity\User;
use tpdoctrine\Entity\Address;

$userRepo = $entityManager->getRepository(User::class);

$user = $userRepo->find(3);

$address = new Address();
$address->setStreet("Champ de Mars, 5 Avenue Anatole");
$address->setZipcode("75017");
$address->setCity("Paris");
$address->setCountry("France");

$user->setAddress($address);

$entityManager->flush();
